<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailModel extends Model
{
    protected $table ="detail_test";
    protected $fillable = [
        'web_id','url','url_parent','title','status', 'isCrawled','created_at','updated_at'
    ];
    public $timestamps= true;
}
