<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaileModel extends Model
{
    protected $table ="failed_url";
    protected $fillable = [
        'web_id','url','url_parent','title','status', 'isCrawled','created_at','updated_at'
    ];
    public $timestamps= true;
}
