<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ApiController extends Controller
{
    public function getIndexWeb()
    {
        $query = DB::table('website')
            ->get()->toArray();
        return DataTables::of($query)
            ->addColumn('check', function ($data) {
                return '<input type="checkbox" data-check=" '.$data->id.' "class="minimal">';
            })
            ->editColumn('status', function ($data) {
                if ($data->status === 0) {
                    return 'パディング';
                } else if ($data->status === 1) {
                    return '読み込み中';
                } else if ($data->status === 2) {
                    return '成功';
                } else {
                    return 'エラー';
                }
            })
            ->addColumn('action', function ($data) {
//                if ($data->status === 0) {
//                    return ' <button class="btn btn-danger" data-id="' . $data->id . '"  onclick = loadUrl(' . $data->id . ') style="margin-right: 15px">
//                                <i class="fa fa-upload"></i> 負荷</button>
//                                  <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDeleteWebsite"
//                                onclick = checkIdExisted(' . $data->id . ')> <i class="fa fa-remove"></i> 削除する</button>';
//
//                } else {
//                    return '<a href="' . route('load_detail', $data->id) . '" class="btn btn-success">
//                                <i class="fa fa-eye"></i> 詳細</a>
//                                <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDeleteWebsite"
//                                onclick = checkIdExisted(' . $data->id . ')> <i class="fa fa-remove"></i> 削除する</button>';
//                }
                return '<a href="' . route('load_detail', $data->id) . '" class="btn btn-success">
                                <i class="fa fa-eye"></i> 詳細</a>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDeleteWebsite"  
                                onclick = checkIdExisted(' . $data->id . ')> <i class="fa fa-remove"></i> 削除する</button>';
            })
            ->rawColumns(['status', 'action','check'])
            ->make(true);
    }

    public function getDetailWeb(Request $id)
    {
        $query = DB::table('website')
            ->where('website.id','=',$id->id)
            ->join('pages_detail', 'website.id', '=', 'pages_detail.parent_id')
            ->get()->toArray();
        $newValues = array();
        foreach ($query as $key => $value){
            $newValues[$key] = $value->id;
        }
        $data = DB::table('pages')
            ->whereIn('web_id',$newValues)
            ->orWhere('web_id','=',$id->id)
            ->get()->toArray();
        return DataTables::of($data)
            ->addColumn('url', function ($data) {
                return '<div class="max-width:200px">
                    <a style="width:35%" href="' . urldecode($data->url) . '">' . urldecode($data->url) . '</a>
                    </div>';
            })
            ->addColumn('fix', function ($data) {
                return '<div class="max-width:35%">
                        <a  style="width:35%" href="' . $data->url_parent . '">' . urldecode($data->url_parent) . '</a>
                        </div>';
            })
            ->addColumn('status', function ($data) {
                if ($data->status == "200") {
                    return '<span class="text-green font-weight-bold">' . $data->status . '</span>';
                } else {
                    return '<span class="text-red">' . $data->status . '</span>';
                }
            })
            ->rawColumns(['status', 'url', 'fix', 'buttons'])
            ->make(true);
    }

    public function getIndexPage(Request $id){
        $query = DB::table('pages_detail')
            ->where('parent_id', $id->id)
            ->get()->toArray();
        return DataTables::of($query)
            ->addColumn('check', function ($data) {
                return '<input type="checkbox" data-check="'.$data->id.' "class="minimal">';
            })
            ->editColumn('status', function ($data) {
                if ($data->status === 0) {
                    return 'パディング';
                } else if ($data->status === 1) {
                    return '読み込み中';
                } else if ($data->status === 2) {
                    return '成功';
                } else {
                    return 'エラー';
                }
            })
            ->addColumn('name_url', function ($data) {
                return '<a  href="'.route('page_detail',$data->id).'"> '.$data->url.'</a>';
            })
            ->addColumn('action', function ($data) {
                if ($data->status === 0) {
                    return ' <button class="btn btn-danger" data-id="' . $data->id . '"  onclick = loadUrlPage(' . $data->id . ') style="margin-right: 15px">
                                <i class="fa fa-upload"></i> 負荷</button>
                                  <a class="btn btn-danger" href="'.route('delete_page',$data->id).'"> <i class="fa fa-remove"></i> 削除する</a>';

                } else {
                    return ' <a class="btn btn-success" href="'.route('page_detail',$data->id).'"> <i class="fa fa-eye"></i> 細部</a>
                            <a class="btn btn-danger" href="'.route('delete_page',$data->id).'"> <i class="fa fa-remove"></i> 削除する</a>';
                }

            })
            ->rawColumns(['status', 'action','check','name_url'])
            ->make(true);
    }
}
