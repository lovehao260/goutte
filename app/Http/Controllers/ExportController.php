<?php

namespace App\Http\Controllers;

use App\TestModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Excel;
class ExportController extends Controller  implements FromCollection,WithHeadings,ShouldAutoSize
{

    public $id;

    function __construct( $request) {
        $this->id=$request;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $query = DB::table('website')
            ->where('website.id','=',$this->id)
            ->join('pages_detail', 'website.id', '=', 'pages_detail.parent_id')
            ->get()->toArray();
        $newValues = array();
        foreach ($query as $key => $value){
            $newValues[$key] = $value->id;
        }
        $data = DB::table('pages')
            ->whereIn('web_id',$newValues)
            ->orWhere('web_id','=',$this->id)
            ->get()->toArray();
        // TODO: Implement collection() method.

        $stt=1;
        foreach ($data as $row) {
            $list[] = array(
                '0' => $stt++,
                '1' => $row->url,
                '2' => $row->title,
                '3' => $row->status,
            );
        }
        return (collect($list));
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        // TODO: Implement headings() method.
            return [
                '#',
                'Url',
                'Title',
                'Status',
            ];
    }

}
