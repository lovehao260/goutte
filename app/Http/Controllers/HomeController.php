<?php

namespace App\Http\Controllers;

use App\FaileModel;
use App\PagesModel;
use Goutte\Client;
use App\TestModel;
use App\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected static $domain_global;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('website.home');
    }

    /**
     * Save website in table.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postAddWeb(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required |unique:website',
            'url' => [
                'required',
                'max:100',
                'unique:website',
                'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',
//                function ($attribute, $value, $fail) {
//                    if ($this->getHttpStatus($value) != '200') {
//                        return $fail('The ' . $attribute . ' Unauthorized');
//                    }
//                }
            ],
        ]);
        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        } else {
            $web = new Website();
            $web->name = $request->name;
            $url = $request->url;
            $url = rtrim($url, "/");
            $web->url = $url;
            $web->save();
            return redirect('/')->with('status', 'Add website Success');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function loadUrl()
    {
        $request = request();
        $id = $request->url_id;
        //$id=66;
        $web = Website::find($id);
        if (isset($request->re_load)) {
            TestModel::where('web_id', $web->id)->delete();
            $web->delete();
        }
        $this->enforcement($web,$id);
        return 1;

    }
    //thu thi
    public function enforcement($web,$id){
        $web->status = 1;
        $web->save();
        $url = $web['url'];
        //$url='https://reviewsnice.com/news/ranksnap-20-pro-review-top-1-google-youtube-at-your-fingertips';
        self::$domain_global = $url;

        TestModel::where('url', $url)->delete();
        $startingLink = TestModel::create([
            'web_id' => $id,
            'url' => $url,
            'isCrawled' => false,
        ]);
        $this->browser($startingLink, $id);
        $web->status = 2;
        $web->save();
    }
    /**
     *Quet cac ul co trong database
     */
    protected function browser($currentUrl, $id)
    {
        $this->getLink($currentUrl, $id);
        try {
            foreach (TestModel::where([
                'isCrawled' => false
            ])->get() as $link) {
                $this->browser($link, $id);
            }
        } catch (\Exception $e) {

        }
    }

    /**
     *Thuc hien lay url luu vao database
     */
    protected function getLink($currentUrl, $id)
    {
        //Check if already crawled
        if (TestModel::where('url', $currentUrl->url)->first()['isCrawled'] == true)
            return true;

        //Visit URL
        $client = new Client();
        $crawler = $client->request('GET', $currentUrl->url);
        $status_code = $client->getResponse()->getStatus();
        if (!$status_code) {
            $fail = new FaileModel();
            $fail['status'] = $status_code;
            $fail['title'] = $crawler->filter('title')->text();
            $fail['isCrawled'] = false;
            $fail->save();
            return true;
        }

        //Check empty
        if ($crawler->filter('a')->count() > 0) {
            //Get Links and Save to array
            $links = $crawler->filter('a')->each(function ($node) {
                if (preg_match('/[#@$]/', $node->attr('href'))) {
                    return false;
                }
                $url =$node->attr('href') ;
                if (empty(parse_url(trim($url))['scheme'])){
                    $url = substr($url, 0, 1) == "/" ?
                        $url = self::$domain_global . $url : $url = self::$domain_global . "/" . $url;
                }
                if (!preg_match('/^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url)) {
                    return false;
                }
                $host = parse_url($url)['host'];

                return compact('url', 'host');
            });
            $null = array_values(array_filter($links));
            $array = array_unique($null, SORT_REGULAR);
            // unset($array['0']);
            // Save to DB if Valid
            foreach ($array as $item) {
                if ($this->isValidUrl($item)) {
                    TestModel::create([
                        'web_id' => $id,
                        'url' => $item['url'],
                        'url_parent' => $currentUrl->url,
                        'isCrawled' => false,
                    ]);
                }
            }
        }
        //Update current url status to crawled

        $currentUrl['status'] = $status_code;
        if ($crawler->filter('a')->count() > 0) {
            $currentUrl['title'] = $crawler->filter('title')->text();
        }
        $currentUrl['isCrawled'] = true;
        $currentUrl->save();

    }

    /**
     *Convert url
     */
    protected function trimUrl($url)
    {
        $url = strtok($url, '#');
        $url = rtrim($url, "/");
        return $url;
    }

    /**
     *Check ton tai cua url va xem url thuoc website do kong
     */
    protected function isValidUrl($domain)
    {
        if (strpos(self::$domain_global, $domain['host']) !== false
            && !TestModel::where('url', $domain['url'])->exists()) {
            return true;
        }
        return false;
    }

    /**
     *Get status url
     */
    function getHttpStatus($url)
    {
        $headers = get_headers($url, 1);
        return intval(substr($headers[0], 9, 3));
    }

    /**
     * Show list url of website
     * @param \Illuminate\Support\Facades\Request
     * @return \Illuminate\Http\Response
     */
    public function getDetailWeb(Request $request)
    {
        $id = $request->id;
        $check = Website::find($id);
        return view('website.detail')->with([
            'id' => $id,
            'check' => $check
        ]);
    }

    /**
     * find website and delete website
     * @param \Illuminate\Support\Facades\Request
     */
    public function deleteWebsite(Request $request)
    {
        $id = $request->id;
        $web = Website::find($id);
        $page=PagesModel::where('parent_id', $id)->get();
        $newValues = array();
        foreach ($page as $key => $value){
            $newValues[$key] = $value->id;
        }
         DB::table('pages')
            ->whereIn('web_id',$newValues)
            ->orWhere('web_id','=',$id)
            ->delete();
        PagesModel::where('parent_id', $id)->delete();
        $web->delete();
        return 1;
    }

    public function export(Request $request)
    {
        $web = Website::find($request->id);
        $name = preg_replace('/\s+/', '', $web->name);
        return Excel::download(new ExportController($request->id), $name . '.xlsx');
    }

    public function loadAll(Request $request)
    {
        if (isset($request->id)){
            TestModel::where('web_id', $request->id)->delete();
            $data = Website::select('id', 'status')->where('id',$request->id)->get();
        }else{
            DB::table('pages')->delete();
            $data = Website::select('id', 'status')->orderBy('created_at','desc')->get();
        }

        foreach ($data as $row) {
            $row['status'] = 0;
            $row->save();
            $id = $row->id;
            $web = Website::find($id);
            $this->enforcement($web,$id);
        }
        return redirect('/')->with('status', 'Add website Success');
    }
}



