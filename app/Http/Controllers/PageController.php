<?php

namespace App\Http\Controllers;

use App\DetailModel;
use App\FaileModel;
use App\PagesModel;
use App\TestModel;
use App\Website;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    protected static $domain_global;

    public function postAddPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => [
                'required',
                'max:100',
                'unique:pages_detail',
                'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'
            ],
        ]);
        if ($validator->fails()) {
            return redirect('/website-detail/'.$request->page_id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $url = $request->url;
            $client = new Client();
            $crawler = $client->request('GET', $url);
            $status = $client->getResponse()->getStatus();
            $url = rtrim($url, "/");
            if ($status > '400') {
                $web = new TestModel();
                $web->url = $url;
                $web->web_id = $request->page_id;
                $web->status = $status;
                $web->isCrawled = true;
                $web->save();
            }else{
                $web = new PagesModel();
                $web->url = $url;
                $web->parent_id = $request->page_id;
                $web->save();
            }
            return redirect()->back()->with('status', 'Add website Success');
        }
    }

    public function loadPage()
    {
        $request = request();
        $id = $request->page_url;
        $web = PagesModel::find($id);
        $this->enforcement($web,$id);
    }
    //thu thi
    public function enforcement($web,$id){
        $web['status'] = 1;
        $web->save();
        $url = $web['url'];
        self::$domain_global = $url;
        $startingLink = TestModel::create([
            'web_id' => $id,
            'url' => $url,
            'isCrawled' => false,
        ]);
        try {
            $this->browser($startingLink, $id);
            $web = PagesModel::find($id);
            $web->status = 2;
            $web->save();
        } catch (\Exception $e) {

        }
    }
    /**
     *Thuc hien lay url luu vao database
     */
    protected function browser($currentUrl, $id)
    {
        //Check if already crawled
        if (TestModel::where([
                'url'=> $currentUrl->url,
                'web_id'=>$id
            ])->first()['isCrawled'] == true)
            return true;
        //Visit URL
        $client = new Client();
        $crawler = $client->request('GET', $currentUrl->url);

        //Check empty
        if ($crawler->filter('a')->count() > 0) {
            //Get Links and Save to array
            $links = $crawler->filter('a')->each(function ($node) {
                if (preg_match('/[#@$]/', $node->attr('href'))) {
                    return false;
                }
                $url = $node->attr('href');
                if (empty(parse_url(trim($url))['scheme'])) {
                    $url = substr($url, 0, 1) == "/" ?
                        $url = self::$domain_global . $url : $url = self::$domain_global . "/" . $url;
                }
                if (!preg_match('/^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url)) {
                    return false;
                }
                $host = parse_url($url)['host'];
                return compact('url', 'host');
            });
            $null = array_values(array_filter($links));
            $array = array_unique($null, SORT_REGULAR);

            // unset($array['0']);
            // Save to DB if Valid
            foreach ($array as $item) {
                if ($this->isValidUrl($item)) {
                    $client_title = new Client();
                    $crawler_ti = $client_title->request('GET', $item['url']);
                    $status_code = $client_title->getResponse()->getStatus();
                    if ($crawler_ti->filter('a')->count() > 0) {
                        $currentUrl_title = $crawler_ti->filter('title')->text();
                    }else{
                        $currentUrl_title=" ";
                    }
                    TestModel::create([
                        'web_id' => $id,
                        'url' => $item['url'],
                        'url_parent' => $currentUrl->url,
                        'status'=>$status_code,
                        'title' => $currentUrl_title,
                        'isCrawled' => false,
                    ]);
                }
            }
        }
        //Update current url status to crawled

        $currentUrl['status'] = $status_code;
        if ($crawler->filter('a')->count() > 0) {
            $currentUrl['title'] = $crawler->filter('title')->text();
        }
        $currentUrl['isCrawled'] = true;
        $currentUrl->save();

    }

    /**
     *Check ton tai cua url va xem url thuoc website do kong
     */
    protected function isValidUrl($domain)
    {
        if (strpos(self::$domain_global, $domain['host']) !== false
            && !TestModel::where('url', $domain['url'])->exists()) {
            return true;
        }
        return false;
    }

    /**
     * Delete page urk
     *
     */
    protected  function deletePage(Request $request){
        $id=$request->id;
        TestModel::where('web_id', $id)->delete();
        $web = PagesModel::find($id);
        $web->delete();
        return redirect()->back();
    }

    public function reloadPage(Request $request){
        TestModel::where('web_id', $request->id)->delete();
        $data = PagesModel::select('id','parent_id', 'status')->where('id',$request->id)->get();
        foreach ($data as $row) {
            $row['status'] = 0;
            $row->save();
            $id = $row->id;
            $web = PagesModel::find($id);
            $this->enforcement($web,$id);
        }

    }

    /**
     *Get status url
     */
    function getHttpStatus($url)
    {
        $headers = get_headers($url, 1);
        return intval(substr($headers[0], 9, 3));
    }

    function detailPage (Request $request){
        $id = $request->id;
        $check = Website::find($id);
        return view('website.page')->with([
            'id' => $id,
            'check' => $check
        ]);
    }

}
