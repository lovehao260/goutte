<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagesModel extends Model
{
    protected $table ="pages_detail";
    protected $fillable = [
        'url','parent_id','status','created_at','updated_at'
    ];
    public $timestamps= true;
}
