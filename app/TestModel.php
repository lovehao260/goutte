<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
    protected $table ="pages";
    protected $fillable = [
        'web_id','url','url_parent','title','status', 'isCrawled','created_at','updated_at'
    ];
    public $timestamps= true;
}
