<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $table ="website";
    protected $fillable = [
        'url','name','status','created_at','updated_at'
    ];
    public $timestamps= true;
}
