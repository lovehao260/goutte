<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default users for Development
        DB::table('users')->delete();
        $users = array(
            array('name' => 'Le Minh Hao','email' => 'haole042@gmail.com', 'password' => bcrypt('hao96969797')),
            array('name' => 'CALC CORPORATION','email' => 'web@calcinc.co.jp', 'password' => bcrypt('calc5344')),
        );

        DB::table('users')->insert($users);
    }
}
