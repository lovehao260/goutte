<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'ホーム',
    'website' => 'ウェブサイトリスト',
    'created' => '作成した',
    'name'=>'名',
    'status'=>'状態',
    'action'=>'アクション',
    'menu'=>'メニュー',
    'submit'=>'提出する',
    'close'=>'閉じる',
    'creat_web'=>'ウェブサイトを作成',
    'logout'=>'ログアウト',
    'success'=>'成功',
    'load'=>'積み込み',
    'wait'=>'パディング',
    'login_s'=>'サインインしてセッションを開始します',
    'sing'=>'サインイン',
    'remember'=>'覚えている',
    'online'=>'オンライン',

];
