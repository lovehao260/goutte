<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/fontawesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('plugins/adminlte/css/adminlte.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
    <!-- Skin ALT Admin -->
    <link rel="stylesheet" href="{{asset('plugins/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
    <!-- Style css -->
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">


    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
@include('layouts.navbar')
<!-- /.navbar -->

    <!-- Main Sidebar Container -->
@include('layouts.sidebar')

<!-- Content Wrapper. Contains page content -->

@yield('content')
<!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-light">
        <!-- Control sidebar content goes here -->

    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>Copyright ©2019 <a href="{{url('/')}}">CALC CORPORATION</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->
<!-- jQuery -->

<script src="{{asset('plugins/jquery/jquery.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -->
<script src="{{asset('plugins/adminlte/js/adminlte.js')}}"></script>


{{--<script src="{{ asset('js/app.js') }}"></script>--}}

<script src="{{asset('plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>

<script src="{{ asset('js/admin.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>



</script>
@stack('page-scripts')
</body>
</html>
