
<section class="main-header ">
    <a href="{{url('/')}}" class="logo" >
        <span class="logo-mini"><b>P</b>S</span>
        <img src="{{asset('img/logo1.png')}}" alt="logo" class="logo-default" style="max-width: 120px;margin-right: 20px">
    </a>
    <nav class="main-header navbar navbar-expand border-bottom navbar-dark">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{url('/')}}" class="nav-link">ホーム</a>
            </li>
        </ul>

    </nav>
</section>
<script>

</script>
