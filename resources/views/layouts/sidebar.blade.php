<!-- ./Vung Header -->
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('img/avatar.jpg')}}" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i>
                    オンライン
                </a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">メニュー</li>
            <li>
                <a href="{{ url('') }}">
                    <i class="fa fa-server"></i> ウェブサイト
                </a>
            </li>
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                            class="fa fa-sign-out"></i>
                    ログアウト
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>

    </section>
    <script>
        $(document).ready(function () {
            var url = window.location;
            var element = $('ul.sidebar-menu a').filter(function () {
                return this.href == url || window.location.pathname == 0;
            });
            $(element).parentsUntil('ul.sidebar-menu', 'li').addClass('active');
        });
    </script>
</aside>