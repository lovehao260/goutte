@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/')}}"
                                                                                      class="text-black">ホーム</a></li>
                            <li class="breadcrumb-item active">
                                詳細リスト
                            </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{session('status')}}</strong>
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">success News Status
                            update
                        </div>
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">

                                        <div class="export">

                                            <a href="{{ route('export',$id) }}"
                                               class="btn btn-info export  float-right m-2" id="export-button">
                                                <i class="fa fa-file-excel-o"></i>
                                                輸出する </a>
                                            <button class="btn btn-danger float-right m-2" data-id="' . $data->id . '"
                                                    onclick=loadUrl({{$id}})>
                                                <i class="fa fa-retweet"></i>
                                                ウェブサイトを読み込む
                                            </button>
                                            <button class="btn btn-dark float-right m-2" data-id="' . $data->id . '"
                                                    onclick=reloadPage()>
                                                <i class="fa fa-retweet"></i>
                                                ロードページ
                                            </button>
                                            <button type="button" class="btn btn-primary  float-right m-2"
                                                    data-toggle="modal"
                                                    data-target="#exampleModal">
                                                <i class="fa fa-plus"></i>
                                                作成した
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div><!-- /.card-body -->
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="pages_list" class="table table-bordered table-hover ">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%">
                                                <input type="checkbox" value="" id="check-all">
                                            </th>
                                            <th>Url</th>
                                            <th style="width: 80px">状態</th>
                                            <th style="width: 150px">
                                                作成した
                                            </th>
                                            <th class="text-center btn-action" style="width:auto">アクション</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="webs_list"
                                           class="table table-bordered table-hover table-striped repos  ">
                                        <thead>
                                        <tr style="width: 100%;">
                                            <th style="width: 3%">#</th>
                                            <th style="width:31%">Url</th>
                                            <th style="width:30%">タイトル</th>
                                            <th class="text-center btn-action" style="width: 5%">
                                                状態
                                            </th>
                                            <th style="width:31%">Url 親</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->


        </section>
        <!-- /.content -->
    </div>
    <div role="status" id="load">
        <div class="icon-load spinner-border text-light"></div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{route('add-pages')}}" method="post" enctype="multipart/form-data"
                      accept-charset="utf-8">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ウェブサイトを作成</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Url</label>
                            <input type="text" value="{{$id}}" name="page_id" hidden>
                            <input type="url"
                                   class="form-control    @if($errors->has('url')) border border-danger @endif"
                                   id="recipient-name" name="url" required value="{{old('url')}}">
                            @if ($errors->has('url')) <span
                                    class="text-danger">{{$errors->first('url')}}</span> @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                        <button type="submit" class="btn btn-primary">提出する</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        $("#check-all").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
        @if (count($errors) > 0)
        $('#exampleModal').modal('show');
                @endif
        let url_jp = "{{asset('plugins/datatables/table.json')}}";
        $(document).ready(function () {
            var t = $('#webs_list').DataTable({
                "ajax": {
                    "url": "{{route('web.detail',$id)}}",
                    "type": 'GET',
                },
                "columns": [
                    {
                        "data": null,


                    },
                    {
                        "data": "url",

                    },
                    {
                        "data": "title",
                    },

                    {
                        "data": "status",
                    },
                    {
                        "data": "fix",
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],
                "order": [[3, 'desc']],
                "autoWidth": false,
                "language": {
                    url: url_jp
                },
                lengthMenu: [20, 50, 100],
            });

            t.on('order.dt search.dt', function () {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

        function loadUrl(id) {
            if (confirm('Are you sure you want to reload all url website ?')) {
                $('#load').show();
                setTimeout(function () {
                    $('#load').hide();
                    location.reload();
                }, 1000);
                request = $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    timeoutSeconds: "10",
                    url: "{{route('load_url')}}",
                    method: "GET",
                    data: {
                        url_id: id,
                        re_load: 1
                    },
                    async: true,
                    error: function (xhr) {
                        $('#load').hide();
                        alert("Error !! URL dont't Crawler or Timeout ");
                        location.reload();
                    },
                }).done(function (data) {

                })
            }
        }

        // Page list

        $("#pages_list").DataTable({
            "processing": true,
            "pageLength": 10,
            "ajax": {
                "url": "{{route('page.index',$id)}}",
                "type": 'GET',
            },
            "columns": [
                {
                    "bSortable": false,
                    "data": "check",
                },
                {
                    "data": "name_url"
                },
                {
                    "data": "status",
                },
                {
                    "data": "created_at",
                    "bSortable": true,
                },
                {
                    "bSortable": false,
                    "data": "action",
                },
            ],
            "language": {
                url: url_jp
            },
            order: [[4, 'desc']],
        });

        function loadUrlPage(id) {
            $('#load').show();
            setTimeout(function () {
                $('#load').hide();
                location.reload();
            }, 1000);
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                timeoutSeconds: "10",
                url: "{{route('load_url_page')}}",
                method: "GET",
                data: {
                    page_url: id
                },
                async: true,
                error: function (xhr) {
                    $('#load').hide();
                    alert("Error !! URL dont't Crawler or Timeout ");
                    location.reload();
                },
            }).done(function (data) {
                console.log(data)
            })
        }

        function reloadPage() {
            if ($(".minimal:checked").length > 0) {
                $('#load').show();
                setTimeout(function () {
                    $('#load').hide();
                    location.reload();
                }, 2000);
                $(".minimal:checked").each(function () {
                    let id = $(this).attr("data-check");
                    request = $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        timeoutSeconds: "10",
                        url: "{{route('reload_page')}}",
                        method: "GET",
                        data: {
                            id: id
                        },
                    }).done(function (data) {
                        console.log(data)
                    });
                });

            }else{
                alert('Please select page reload');
            }
        }
    </script>

@endpush