@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/')}}"
                                   class="text-black">ホーム</a>
                            </li>
                            <li class="breadcrumb-item active">
                                ウェブサイトリスト
                            </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{session('status')}}</strong>
                            </div>
                        @endif
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary  float-right m-2"
                                                data-toggle="modal"
                                                data-target="#exampleModal">
                                            <i class="fa fa-plus"></i>
                                            作成した
                                        </button>
                                        <button type="button" class="btn btn-dark float-right m-2 text-white"
                                                onclick="loadAll()">
                                            <i class="fa fa-loading"></i>
                                            Reload
                                        </button>

                                    </div>
                                </div>
                            </div><!-- /.card-body -->
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="webs_list" class="table table-bordered table-hover ">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%">
                                                    <input type="checkbox" value="" id="check-all">

                                            </th>
                                            <th>名</th>
                                            <th>Url</th>
                                            <th style="width: 80px">状態</th>
                                            <th style="width: 150px">
                                                作成した
                                            </th>
                                            <th class="text-center btn-action" style="width:auto">アクション</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->
            <!-- The Modal -->
            <!-- The Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{route('add-web')}}" method="post" enctype="multipart/form-data"
                              accept-charset="utf-8">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">ウェブサイトを作成</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">名</label>
                                    <input type="text"
                                           class="form-control    @if($errors->has('name')) border border-danger @endif"
                                           id="recipient-name" name="name" required value="{{old('name')}}">
                                    @if ($errors->has('name'))
                                        <div
                                                class="text-danger">{{$errors->first('name')}}</div> @endif
                                    <label for="recipient-name" class="col-form-label">Url</label>
                                    <input type="url"
                                           class="form-control    @if($errors->has('url')) border border-danger @endif"
                                           id="recipient-name" name="url" required value="{{old('url')}}">
                                    @if ($errors->has('url')) <span
                                            class="text-danger">{{$errors->first('url')}}</span> @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                                <button type="submit" class="btn btn-primary">提出する</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- confirm dialog-->
            <div class="modal fade" id="confirmDeleteWebsite" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            ウェブサイトの削除を確認
                        </div>
                        <div id="confirmMessage" class="modal-body">
                            ウェブサイトを削除したい
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                                    onclick="deleteWebsite($(this).val())">
                                削除する
                            </button>
                            <button type="button" id="confirmCancel" class="btn btn-cancel" data-dismiss="modal">
                                キャンセル
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <div role="status" id="load">
        <div class="icon-load spinner-border text-light"></div>
    </div>
@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        let url_jp = "{{asset('plugins/datatables/table.json')}}";
        @if (count($errors) > 0)
        $('#exampleModal').modal('show');
        @endif
            $.fn.dataTable.ext.errMode = 'throw';
        $("#webs_list").DataTable({
            "processing": true,
            "pageLength": 10,
            "ajax": {
                "url": "{{route('web.index')}}",
                "type": 'GET',
            },
            "columns": [
                {
                    "bSortable": false,
                    "data": "check",
                },
                {
                    "data": "name"
                },

                {
                    "data": "url"
                },
                {
                    "data": "status",
                },
                {
                    "data": "created_at",
                    "bSortable": true,
                },
                {
                    "bSortable": false,
                    "data": "action",
                },
            ],
            "language": {
                url: url_jp
            },
            order: [[4, 'desc']],
        });
        $("#check-all").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
        function loadUrl(id) {
            $('#load').show();
            setTimeout(function () {
                $('#load').hide();
                location.reload();
            }, 3000);
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                timeoutSeconds: "10",
                url: "{{route('load_url')}}",
                method: "GET",
                data: {
                    url_id: id
                },
                async: true,
                error: function (xhr) {
                    $('#load').hide();
                    alert("Error !! URL dont't Crawler or Timeout ");
                    location.reload();
                },
            }).done(function (data) {

            })
        }

        function loadAll() {
            $('#load').show();
            setTimeout(function () {
                $('#load').hide();
                location.reload();
            }, 3000);
            if ($(".minimal:checked").length > 0) {
                $(".minimal:checked").each(function () {
                    let id = $(this).attr("data-check");
                    request = $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('load_all')}}",
                        method: "GET",
                        data: {
                            id: id,
                        }
                    })
                });
            }else{
                request = $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('load_all')}}",
                    method: "GET",
                    async: true,
                    error: function (xhr) {
                        $('#load').hide();
                        alert("Error !! URL dont't Crawler or Timeout ");
                        location.reload();
                    },
                }).done(function (data) {

                })
            }
        }

        function checkIdExisted(id) {
            $('#btnConfirmDelete').val(id);
        }

        function deleteWebsite(id) {
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('delete_web')}}",
                method: "get",
                data: {
                    id: id,
                }
            }).done(function (data) {
                    if (data === "1") {
                        location.reload();
                    } else {
                        alert('Failure Role User ')
                    }
                }
            )

        }

    </script>

@endpush