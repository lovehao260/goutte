@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/')}}"
                                                                                      class="text-black">ホーム</a></li>
                            <li class="breadcrumb-item active">
                                詳細リスト
                            </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{session('status')}}</strong>
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">success News Status
                            update
                        </div>
                        <div class="card card-primary card-outline">
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="webs_list"
                                           class="table table-bordered table-hover table-striped repos  ">
                                        <thead>
                                        <tr style="width: 100%;">
                                            <th style="width: 3%">#</th>
                                            <th style="width:31%">Url</th>
                                            <th style="width:30%">タイトル</th>
                                            <th class="text-center btn-action" style="width: 5%">
                                                状態
                                            </th>
                                            <th style="width:31%">Url 親</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->


        </section>
        <!-- /.content -->
@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        let url_jp = "{{asset('plugins/datatables/table.json')}}";
        $(document).ready(function () {
            var t = $('#webs_list').DataTable({
                "ajax": {
                    "url": "{{route('web.detail',$id)}}",
                    "type": 'GET',
                },
                "columns": [
                    {
                        "data": null,


                    },
                    {
                        "data": "url",

                    },
                    {
                        "data": "title",
                    },

                    {
                        "data": "status",
                    },
                    {
                        "data": "fix",
                    },
                ],
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],
                "order": [[3, 'desc']],
                "autoWidth": false,
                "language": {
                    url: url_jp
                },
                lengthMenu: [20, 50, 100],
            });

            t.on('order.dt search.dt', function () {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });


    </script>

@endpush