<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/index-website','ApiController@getIndexWeb')->name('web.index');
Route::get('/detail-website/{id}','ApiController@getDetailWeb')->name('web.detail');

Route::get('/index-page/{id}','ApiController@getIndexPage')->name('page.index');
Route::get('/page_detail/{id}','ApiController@detailPage')->name('detail_page');