<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'as'=>'home',
    'uses'=>'HomeController@index'
]);
Auth::routes();
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test','HomeController@test');

Route::post('/add-website','HomeController@postAddWeb')->name('add-web');
Route::get('/load-website','HomeController@loadUrl')->name('load_url');
Route::get('/website-detail/{id}','HomeController@getDetailWeb')->name('load_detail');
Route::get('/delete-website','HomeController@deleteWebsite')->name('delete_web');
Route::get('/load-all', 'HomeController@loadAll')->name('load_all');

Route::get('/export/{id}', 'HomeController@Export')->name('export');


Route::post('/add-website-page','PageController@postAddPage')->name('add-pages');
Route::get('/load-page','PageController@loadPage')->name('load_url_page');
Route::get('/delete-page/{id}','PageController@deletePage')->name('delete_page');
Route::get('/page_detail/{id}','PageController@detailPage')->name('page_detail');
Route::get('/reload-page','PageController@reloadPage')->name('reload_page');